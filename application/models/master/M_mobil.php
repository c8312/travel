<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_mobil extends CI_Model
{
    public function get($table, $data = null, $where = null)
    {
        if ($data != null) {
            return $this->db->get_where($table, $data)->row_array();
        } else {
            return $this->db->get_where($table, $where)->result_array();
        }
    }

    public function get_data_mobil()
    {
        $data = $this->db->query('SELECT * FROM mobil_master')->result_array();
        return $data;
    }

    

    public function insert($table, $data, $batch = false)
    {
        return $batch ? $this->db->insert_batch($table, $data) : $this->db->insert($table, $data);
    }

    public function deleteData($table, $primary, $id_lahan)
    {
        return $this->db->delete($table, [$primary => $id_lahan]);
    }

    public function edit_mobil($params, $id_mobil)
    {
        $this->db->where('id_mobil', $id_mobil);
        return $this->db->update('mobil_master', $params);
    }

    public function get_detail_mobil($id_mobil)
    {
        $this->db->select('*');
        $this->db->where('a.id_mobil', $id_mobil);
        return $this->db->get('mobil_master a')->row_array();
    }

    

    
}
