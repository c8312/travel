<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_wisata extends CI_Model
{
    public function get($table, $data = null, $where = null)
    {
        if ($data != null) {
            return $this->db->get_where($table, $data)->row_array();
        } else {
            return $this->db->get_where($table, $where)->result_array();
        }
    }

    public function get_data_wisata()
    {
        $data = $this->db->query('SELECT * FROM wisata')->result_array();
        return $data;
    }

    public function get_data_wisata_portal()
    {
        $data = $this->db->query('
            SELECT * FROM wisata a
            INNER JOIN gambar_wisata b ON a.id_wisata_img = b.id_wisata_img
            GROUP BY a.id_wisata')->result_array();
        return $data;
    }

    public function insert($table, $data, $batch = false)
    {
        return $batch ? $this->db->insert_batch($table, $data) : $this->db->insert($table, $data);
    }

    public function deleteData($table, $primary, $id)
    {
        return $this->db->delete($table, [$primary => $id]);
    }

    public function edit_wisata($params, $id_wisata)
    {
        $this->db->where('id_wisata', $id_wisata);
        return $this->db->update('wisata', $params);
    }

    public function get_detail_wisata($id_wisata)
    {
        $data = $this->db->query('SELECT * FROM wisata WHERE id_wisata = "'.$id_wisata.'"')->row_array();
        $data['list_gambar'] = $this->db->query('
            SELECT * FROM gambar_wisata 
            WHERE id_wisata_img = "'.$data["id_wisata_img"].'"'
        )->result_array();
        return $data;
    }

    

    
}
