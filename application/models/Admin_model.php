<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function get($table, $data = null, $where = null)
    {
        if ($data != null) {
            return $this->db->get_where($table, $data)->row_array();
        } else {
            return $this->db->get_where($table, $where)->result_array();
        }
    }

    public function get_data_warga()
    {
        $data = $this->db->query('SELECT a.*, res.jumlah FROM warga a
        LEFT JOIN (SELECT nik, COUNT(*) as jumlah FROM USER GROUP BY nik) res
        ON a.nik = res.nik')->result_array();
        return $data;
    }

    public function get_data_warga_add_lahan()
    {
        $data = $this->db->query('SELECT a.*, res.jumlah FROM warga a
        LEFT JOIN (SELECT nik, role, COUNT(*) as jumlah FROM USER GROUP BY nik) res
        ON a.nik = res.nik WHERE res.role = "user"')->result_array();
        return $data;
    }

    public function get_data_lahan()
    {
        $this->db->select('*');
        $this->db->join('lahan b', 'a.id_lahan = b.id_lahan');
        $this->db->join('lahan_kering c', 'b.id_lahan = c.id_lahan');
        $this->db->join('lahan_sawah d', 'b.id_lahan = d.id_lahan');
        $this->db->where('status_kepemilikan', 'ya');
        return $this->db->get('log_lahan a')->result_array();
    }

    public function get_detail_warga($nik)
    {
        $this->db->select('*');
        $this->db->where('a.nik', $nik);
        return $this->db->get('warga a')->row_array();
    }

    public function get_detail_lahan($id_lahan)
    {
        $this->db->select('*');
        $this->db->join('lahan_kering b', 'a.id_lahan = b.id_lahan');
        $this->db->join('lahan_sawah c', 'a.id_lahan = c.id_lahan');
        $this->db->join('log_lahan d', 'a.id_lahan = d.id_lahan');
        $this->db->join('warga e', 'd.nik = e.nik');
        $this->db->where('a.id_lahan', $id_lahan);
        return $this->db->get('lahan a')->row_array();
    }

    public function get_detail_user($id)
    {
        $this->db->select('*');
        $this->db->join('warga b', 'a.nik = b.nik');
        $this->db->where('a.id', $id);
        return $this->db->get('user a')->row_array();
    }

    public function get_detail_lahan_by_nomor_nik($nomor_lahan, $nik, $id_lahan)
    {
        $this->db->select('*');
        $this->db->join('lahan b', 'a.id_lahan = b.id_lahan');
        $this->db->join('lahan_kering c', 'b.id_lahan = c.id_lahan');
        $this->db->join('lahan_sawah d', 'b.id_lahan = d.id_lahan');
        $this->db->join('warga e', 'a.nik = e.nik');
        $array = array('nomor_lahan' => $nomor_lahan, 'a.nik' => $nik, 'a.id_lahan' => $id_lahan);
        $this->db->where($array);
        return $this->db->get('log_lahan a')->row_array();
    }


    public function edit_warga($params, $nik)
    {
        $this->db->where('nik', $nik);
        return $this->db->update('warga', $params);
    }

    public function edit_lahan($params, $id_lahan)
    {
        $this->db->where('id_lahan', $id_lahan);
        return $this->db->update('lahan', $params);
    }

    public function edit_lahan_kering($params, $id_lahan)
    {
        $this->db->where('id_lahan', $id_lahan);
        return $this->db->update('lahan_kering', $params);
    }

    public function edit_lahan_sawah($params, $id_lahan)
    {
        $this->db->where('id_lahan', $id_lahan);
        return $this->db->update('lahan_sawah', $params);
    }

    public function edit_log_lahan($params, $id_log)
    {
        $this->db->where('id_log', $id_log);
        return $this->db->update('log_lahan', $params);
    }

    public function insert($table, $data, $batch = false)
    {
        return $batch ? $this->db->insert_batch($table, $data) : $this->db->insert($table, $data);
    }

    public function deleteData($table, $primary, $id_lahan)
    {
        return $this->db->delete($table, [$primary => $id_lahan]);
    }

    public function get_data_lahan_user()
    {
        $this->db->select('*');
        $this->db->join('warga b', 'a.nik = b.nik');
        $this->db->where('status_kepemilikan', 'ya');
        return $this->db->get('log_lahan a')->result_array();
    }

    public function get_all_log_mutasi()
    {
        $result = $this->db->query('SELECT a.nomor_lahan FROM log_lahan a GROUP BY a.nomor_lahan')->result_array();
        foreach ($result as $key => $value) {
            $id_lahan = $this->db->query('SELECT id_lahan FROM log_lahan WHERE nomor_lahan = "'.$value["nomor_lahan"].'" ORDER BY id_lahan DESC LIMIT 1')->row_array();
            $result[$key]['id_lahan'] = $id_lahan['id_lahan'];
            $result[$key]['hasil_log'] = $this->get_log_lahan_by_no_lahan($value['nomor_lahan']);
            $result[$key]['pemilik_saat_ini'] = $this->get_nama_lahan($value['nomor_lahan']);
        }
        return $result;
    }

    public function get_nama_lahan($nomor_lahan)
    {
        $data = $this->db->query('SELECT nama FROM log_lahan a INNER JOIN warga b ON a.nik = b.nik WHERE status_kepemilikan = "ya" AND a.nomor_lahan = "' . $nomor_lahan . '"')->row_array();
        return $data['nama'];
    }

    public function get_log_lahan_by_no_lahan($nomor_lahan)
    {
        return $this->db->query('SELECT * FROM log_lahan a 
        INNER JOIN warga b ON a.nik = b.nik 
        INNER JOIN lahan c ON a.id_lahan = c.id_lahan 
        INNER JOIN lahan_sawah d ON c.id_lahan = d.id_lahan
        INNER JOIN lahan_kering e ON c.id_lahan = e.id_lahan
        WHERE a.nomor_lahan = "' . $nomor_lahan . '" ORDER BY a.date DESC')->result_array();
    }

    public function summary()
    {
        $data = $this->db->query("
            SELECT 
            (SELECT COUNT(*)'pesanan' FROM pesanan)'pesanan', 
            (SELECT COUNT(*)'wisata' FROM wisata)'wisata', 
            (SELECT COUNT(*)'user' FROM USER WHERE role = 'user')'user',
            (SELECT COUNT(*)'mobil' FROM mobil_master )'mobil'
            ")->row_array();
        return $data;
    }

    public function get_data_user()
    {
        $data = $this->db->query('SELECT * FROM USER a INNER JOIN warga b ON a.nik = b.nik')->result_array();
        return $data;
    }

    public function get_data_warga_not_user()
    {
        $data = $this->db->query('SELECT a.* FROM warga a WHERE a.nik NOT IN (SELECT nik FROM USER)')->result_array();
        return $data;
    }

    public function edit_user($params, $id)
    {
        $this->db->where('id', $id);
        return $this->db->update('user', $params);
    }

    
}
