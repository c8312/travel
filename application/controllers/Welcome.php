<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('master/M_mobil');
        $this->load->model('master/M_wisata');
    }

	public function index()
	{
		// get data wisata
		$data['rs_wisata'] = $this->M_wisata->get_data_wisata_portal();
		$this->load->view('index', $data);
	}

	public function detail($id_wisata)
	{	
		// get detail wisata
		$detail = $this->M_wisata->get_detail_wisata($id_wisata);
		$data['detail'] = $detail;
		// get mobil
		$data['rs_mobil'] = $this->M_mobil->get_data_mobil();
		$this->load->view('detail', $data);
	}
}
