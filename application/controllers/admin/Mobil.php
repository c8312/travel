<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mobil extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        cek_login();
        $data_user = $this->session->userdata('login_session');
        if ($data_user['role'] != 'admin') {
            redirect('error_404');
        }
        $this->load->model('master/M_mobil');
    }

    public function index()
    {
        $data_user = $this->session->userdata('login_session');
        $data['title'] = "Mobil";
        $data['user_nm'] = $data_user['user_nm'];
        $data['role'] = $data_user['role'];
        $data['rs_mobil'] = $this->M_mobil->get_data_mobil();
        $this->load->view('mobil/index', $data);
    }

    public function add_process()
    {
        $this->form_validation->set_rules('nama_mobil', 'Nama Mobil', 'required|trim');
        $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|trim');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required|trim');
        $this->form_validation->set_rules('sewa', 'Sewa', 'required|trim');
        // 
        if ($this->form_validation->run() == false) {
            set_pesan('data gagal disimpan', false);
            $data_user = $this->session->userdata('login_session');
            $data['user_nm'] = $data_user['user_nm'];
            $data['title'] = 'Tambah User';
            redirect('admin/mobil');
        } else {
            $input = $this->input->post(null, true);
            // 
            $file_name = str_replace('.', '', microtime(true));
            $config['upload_path']          = FCPATH.'/uploads/';
            $config['allowed_types']        = 'jpg|jpeg|png';
            // $config['max_size']             = 1024;
            $config['file_name']            = $file_name;
            $this->load->library('upload', $config);
            $uploaded = $this->upload->do_upload('img');
            $datas = $this->upload->data();
            if(!$uploaded) {
                set_pesan($this->upload->display_errors(), false);
                redirect('admin/mobil');
            }

            // add data
            $mobil = [
                    'nama_mobil' => $input['nama_mobil'], 'kapasitas' => $input['kapasitas'],
                    'tahun' => $input['tahun'],
                    'sewa' => $input['sewa'], 'img' => $datas['file_name']
                ];
            // insert user
            $query = $this->M_mobil->insert('mobil_master', $mobil);
            if ($query) {
                set_pesan('daftar berhasil disimpan');
                redirect('admin/mobil');
            } else {
                set_pesan('gagal menyimpan ke database', false);
                redirect('admin/mobil');
            }
        }
    }

    public function detail_mobil()
    {
        $id_mobil = $this->input->post('id_mobil', true);
        $data = $this->M_mobil->get_detail_mobil($id_mobil);
        echo json_encode($data);
    }

    public function edit_process()
    {
        $this->form_validation->set_rules('nama_mobil', 'Nama Mobil', 'required|trim');
        $this->form_validation->set_rules('kapasitas', 'Kapasitas', 'required|trim');
        $this->form_validation->set_rules('tahun', 'Tahun', 'required|trim');
        $this->form_validation->set_rules('sewa', 'Sewa', 'required|trim');
        // 
        if ($this->form_validation->run() == false) {
            set_pesan('data gagal disimpan', false);
            $data_user = $this->session->userdata('login_session');
            $data['user_nm'] = $data_user['user_nm'];
            $data['title'] = 'Tambah User';
            redirect('admin/mobil');
        } else {
            $input = $this->input->post(null, true);
            $file_name = $input['img_lama'];
            if (!empty($_FILES['img_baru']['name'])) {
                $file_name = str_replace('.', '', microtime(true));
                $config['upload_path']          = FCPATH.'/uploads/';
                $config['allowed_types']        = 'jpg|jpeg|png';
                // $config['max_size']             = 1024;
                $config['file_name']            = $file_name;
                $this->load->library('upload', $config);
                $uploaded = $this->upload->do_upload('img_baru');

                $datas = $this->upload->data();
                $file_name = $datas['file_name'];
                if(!$uploaded) {
                    set_pesan($this->upload->display_errors(), false);
                    redirect('admin/mobil');
                }
            }
            // params update
            $mobil = [
                    'nama_mobil' => $input['nama_mobil'], 'kapasitas' => $input['kapasitas'],
                    'tahun' => $input['tahun'],
                    'sewa' => $input['sewa'], 'img' => $file_name
                ];
            $update_user = $this->M_mobil->edit_mobil($mobil, $input['id_mobil']);
            if ($update_user) {
                // delete file
                $base_url = base_url();
                unlink($base_url.'uploads/'.$input['img_lama']);
                // echo $base_url.'uploads/'.$input['img_lama'];
                // die;
                set_pesan('daftar berhasil di edit.');
                redirect('admin/mobil');
            } else {
                set_pesan('gagal edit', false);
                redirect('admin/mobil');
            }
        }
    }
}
