<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        cek_login();
        $data_user = $this->session->userdata('login_session');
        if ($data_user['role'] != 'admin') {
            redirect('error_404');
        }
        $this->load->model('Admin_model', 'admin');
    }

    public function index()
    {
        $data_user = $this->session->userdata('login_session');
        $data['title'] = "Dashboard";
        $data['user_nm'] = $data_user['user_nm'];
        $data['role'] = $data_user['role'];
        $data['summary'] = $this->admin->summary();
        $this->load->view('dashboard/index', $data);
    }
}
