<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('Auth_model', 'auth');
        $this->load->model('Admin_model', 'admin');
    }

    private function _has_login()
    {
        if ($this->session->has_userdata('login_session')) {
            $data_user = $this->session->userdata('login_session');
            if ($data_user['role'] == 'user') {
                redirect('user/dashboard');
            } else {
                redirect('admin/dashboard');
            }
        }
    }

    public function index()
    {
        $this->_has_login();

        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Login Aplikasi';
            $this->load->view('auth/login');
        } else {

            $input = $this->input->post(null, true);

            $cek_username = $this->auth->cek_username($input['username']);
            if ($cek_username > 0) {
                $password = $this->auth->get_password($input['username']);
                if (password_verify($input['password'], $password)) {
                    $user_db = $this->auth->userdata($input['username']);
                    $userdata = [
                        'user'  => $user_db['id_user'],
                        'role'  => $user_db['role'],
                        'user_nm'  => $user_db['nama'],
                        'nik'  => $user_db['nik'],
                        'timestamp' => time()
                    ];
                    $this->session->set_userdata('login_session', $userdata);
                    if ($user_db['role'] == 'user') {
                        redirect('user/dashboard');
                    } else {
                        redirect('admin/dashboard');
                    }
                } else {
                    set_pesan('password salah', false);
                    redirect('admin/auth');
                }
            } else {
                set_pesan('username belum terdaftar', false);
                redirect('admin/auth');
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('login_session');

        set_pesan('anda telah berhasil logout');
        redirect('admin/auth');
    }


    public function register()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim|is_unique[user.username]|alpha_numeric');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[3]|trim');
        $this->form_validation->set_rules('password2', 'Konfirmasi Password', 'matches[password]|trim');
        $this->form_validation->set_rules('nama', 'Nama', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');

        if ($this->form_validation->run() == false) {
            $data['title'] = 'Buat Akun';
            $this->load->view('auth/register', $data);
        } else {
            $input = $this->input->post(null, true);
            $user = [
                'nama' => $input['nama'], 'username' => $input['username'],
                'role' => 'admin', 'password' => password_hash($input['password'], PASSWORD_DEFAULT),
                'alamat' => $input['alamat'], 'foto' => 'user.png', 'created_at' => time()
            ];
            // insert user
            $query = $this->admin->insert('user', $user);
            if ($query) {
                set_pesan('daftar berhasil. Silahkan masuk menggunakan username dan password.');
                redirect('admin/auth');
            } else {
                set_pesan('gagal menyimpan ke database', false);
                redirect('auth/register');
            }
        }
    }
}
