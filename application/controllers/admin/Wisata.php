<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wisata extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        cek_login();
        $data_user = $this->session->userdata('login_session');
        if ($data_user['role'] != 'admin') {
            redirect('error_404');
        }
        $this->load->model('master/M_wisata');
    }

    public function index()
    {
        $data_user = $this->session->userdata('login_session');
        $data['title'] = "Wisata";
        $data['user_nm'] = $data_user['user_nm'];
        $data['role'] = $data_user['role'];
        $data['rs_wisata'] = $this->M_wisata->get_data_wisata();
        $this->load->view('wisata/index', $data);
    }

    public function add()
    {
        $data_user = $this->session->userdata('login_session');
        $data['title'] = "Wisata";
        $data['user_nm'] = $data_user['user_nm'];
        $data['role'] = $data_user['role'];
        $this->load->view('wisata/add', $data);
        // redirect('admin/wisata');
    }

    public function add_process()
    {
        $this->form_validation->set_rules('nama_wisata', 'Nama Wisata', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('biaya', 'Biaya', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        // 
        if ($this->form_validation->run() == false) {
            set_pesan('data gagal disimpan', false);
            $data_user = $this->session->userdata('login_session');
            $data['title'] = "Wisata";
            $data['user_nm'] = $data_user['user_nm'];
            $data['role'] = $data_user['role'];
            $this->load->view('wisata/add', $data);
            // redirect('admin/wisata');
        } else {
            $input = $this->input->post(null, true);
            // id 
            $id_wisata_img = str_replace('.', '', microtime(true));
            // add data
            $wisata = [
                    'nama_wisata' => $input['nama_wisata'], 'alamat' => $input['alamat'],
                    'biaya' => $input['biaya'],
                    'deskripsi' => $input['deskripsi'], 'id_wisata_img' => $id_wisata_img
                ];
            // insert wisata
            $query = $this->M_wisata->insert('wisata', $wisata);
            if ($query) {
                // insert gambar
                for ($i=0; $i <count($_FILES['img']['name']) ; $i++) { 
                    
                    $_FILES['file']['name'] = $_FILES['img']['name'][$i];
                    $_FILES['file']['type'] = $_FILES['img']['type'][$i];
                    $_FILES['file']['tmp_name'] = $_FILES['img']['tmp_name'][$i];
                    $_FILES['file']['error'] = $_FILES['img']['error'][$i];
                    $_FILES['file']['size'] = $_FILES['img']['size'][$i];
                   
                    $file_name = $id_wisata_img.$i;
                    $config['upload_path']          = FCPATH.'/uploads/';
                    $config['allowed_types']        = 'jpg|jpeg|png';
                    // $config['max_size']             = 1024;
                    $config['file_name']            = $file_name;
                    $this->load->library('upload', $config);
                    $uploaded = $this->upload->do_upload('file');

                    $datas = $this->upload->data();
                    $file_name = $datas['file_name'];
                    if(!$uploaded) {
                        set_pesan($this->upload->display_errors(), false);
                        redirect('admin/wisata');
                    } else{
                        $img_file = [
                            'id_wisata_img' => $id_wisata_img, 'img' => $file_name
                        ];
                        // insert 
                        $query = $this->M_wisata->insert('gambar_wisata', $img_file);
                    }

                }
                set_pesan('daftar berhasil disimpan');
                redirect('admin/wisata');
            } else {
                set_pesan('gagal menyimpan ke database', false);
                redirect('admin/wisata');
            }
        }
    }

    public function lihat($id_wisata ='')
    {
        $detail = $this->M_wisata->get_detail_wisata($id_wisata);
        $data_user = $this->session->userdata('login_session');
        $data['title'] = "Wisata";
        $data['user_nm'] = $data_user['user_nm'];
        $data['role'] = $data_user['role'];
        $data['detail'] = $detail;
        $this->load->view('wisata/lihat', $data);
    }

    public function edit($id_wisata ='')
    {
        $detail = $this->M_wisata->get_detail_wisata($id_wisata);
        $data_user = $this->session->userdata('login_session');
        $data['title'] = "Wisata";
        $data['user_nm'] = $data_user['user_nm'];
        $data['role'] = $data_user['role'];
        $data['detail'] = $detail;
        $this->load->view('wisata/edit', $data);
    }



    public function edit_process()
    {
        $this->form_validation->set_rules('nama_wisata', 'Nama Wisata', 'required|trim');
        $this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
        $this->form_validation->set_rules('biaya', 'Biaya', 'required|trim');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required|trim');
        // 
        if ($this->form_validation->run() == false) {
            set_pesan('data gagal disimpan', false);
            $data_user = $this->session->userdata('login_session');
            $data['title'] = "Wisata";
            $data['user_nm'] = $data_user['user_nm'];
            $data['role'] = $data_user['role'];
            $this->load->view('wisata/edit', $data);
            // redirect('admin/wisata');
        } else {
            $input = $this->input->post(null, true);
            // id 
            $id_wisata_img = str_replace('.', '', microtime(true));
            // add data
            $wisata = [
                    'nama_wisata' => $input['nama_wisata'], 'alamat' => $input['alamat'],
                    'biaya' => $input['biaya'],
                    'deskripsi' => $input['deskripsi']
                ];
            // insert wisata
            $query = $this->M_wisata->edit_wisata($wisata, $input['id_wisata']);
            if ($query) {
                // insert gambar jika ada
                if (!empty($_FILES['img']['name'])) {
                    for ($i=0; $i <count($_FILES['img']['name']) ; $i++) { 
                        $_FILES['file']['name'] = $_FILES['img']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['img']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['img']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['img']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['img']['size'][$i];
                       
                        $file_name = str_replace('.', '', microtime(true)).$i;
                        $config['upload_path']          = FCPATH.'/uploads/';
                        $config['allowed_types']        = 'jpg|jpeg|png';
                        // $config['max_size']             = 1024;
                        $config['file_name']            = $file_name;
                        $this->load->library('upload', $config);
                        $uploaded = $this->upload->do_upload('file');

                        $datas = $this->upload->data();
                        $file_name = $datas['file_name'];
                        if(!$uploaded) {
                            set_pesan($this->upload->display_errors(), false);
                            redirect('admin/wisata');
                        } else{
                            $img_file = [
                                'id_wisata_img' => $input['id_wisata_img'], 'img' => $file_name
                            ];
                            // insert 
                            $query = $this->M_wisata->insert('gambar_wisata', $img_file);
                        }
                    }
                }
                set_pesan('data berhasil diubah');
                redirect('admin/wisata');
            } else {
                set_pesan('gagal menyimpan ke database', false);
                redirect('admin/wisata');
            }
        }
    }

    // delete gambar
    public function delete_gambar($id_gambar='', $id_wisata, $img)
    {
        if($this->M_wisata->deleteData('gambar_wisata', 'id_gambar', $id_gambar)) {
            $folder = FCPATH.'/uploads/'.$img;
            unlink($folder);
            // 
            set_pesan('data berhasil hapus');
            redirect('admin/wisata/edit/'.$id_wisata);
        } else {
            set_pesan('gagal hapus', false);
            redirect('admin/wisata/edit/'.$id_wisata);
        }
    }

    // delete
    public function delete($id_wisata='', $id_wisata_img)
    {
        if($this->M_wisata->deleteData('wisata', 'id_wisata', $id_wisata)) {
            $rs_img = $this->db->query('SELECT id_gambar, img FROM gambar_wisata WHERE id_wisata_img = "'.$id_wisata_img.'"')->result_array();
            foreach ($rs_img as $key => $value) {
                $folder = FCPATH.'/uploads/'.$value['img'];
                $this->M_wisata->deleteData('gambar_wisata', 'id_gambar', $value['id_gambar']);
                unlink($folder);
            }
            // 
            set_pesan('data berhasil hapus');
            redirect('admin/wisata');
        } else {
            set_pesan('gagal hapus', false);
            redirect('admin/wisata');
        }
        
    }

    
}
