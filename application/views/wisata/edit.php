<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<!-- header -->
<?php $ci = $ci = get_instance();
$ci->load->view('templates/header'); ?>
<!-- header -->

<script type="text/javascript" src="<?= base_url() ?>admin/assets/vendor/ckeditor/ckeditor.js"></script>
<!-- Container Fluid-->
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Master Wisata</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item active" aria-current="page">Wisata</li>
        </ol>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
            <!-- Horizontal Form -->
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Edit data wisata</h6>
                  <button onClick="location='<?= base_url() ?>admin/wisata'" type="button" class="btn btn-primary mb-1"><i class="fas fa-arrow-left"></i> Kembali</button>
                </div>
                <div class="card-body">
                    <?= $this->session->flashdata('pesan'); ?>
                    <?php unset($_SESSION['pesan']) ?>
                  <form action="<?= base_url() ?>admin/wisata/edit_process" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_wisata" value="<?= $detail['id_wisata'] ?>">
                    <input type="hidden" name="id_wisata_img" value="<?= $detail['id_wisata_img'] ?>">
                    <div class="form-group row">
                      <label for="wisata" class="col-sm-3 col-form-label">Nama Wisata</label>
                      <div class="col-sm-9">
                        <input type="text" name="nama_wisata" class="form-control" id="nama_wisata" placeholder="Nama tempat Wisata" value="<?= $detail['nama_wisata']; ?>" required>
                            <?= form_error('nama_wisata', '<small class="text-danger">', '</small>'); ?>
                    </div>
                    </div>
                    <div class="form-group row">
                      <label for="wisata" class="col-sm-3 col-form-label">Alamat Wisata</label>
                      <div class="col-sm-9">
                        <input type="text" name="alamat" class="form-control" id="alamat" placeholder="Alamat tempat Wisata" value="<?= $detail['alamat']; ?>" required>
                            <?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label for="wisata" class="col-sm-3 col-form-label">Biaya Wisata</label>
                      <div class="col-sm-9">
                        <input type="number" name="biaya" class="form-control" id="biaya" placeholder="Biaya Wisata" value="<?= $detail['biaya']; ?>" required>
                            <?= form_error('biaya', '<small class="text-danger">', '</small>'); ?>
                      </div>
                    </div>
                    <div class="form-group row">
                        <label for="wisata" class="col-sm-3 col-form-label">Deskripsi</label>
                        <div class="col-sm-9">
                            <textarea class="ckeditor" name="deskripsi" id="ckedtor"><?= $detail['deskripsi']; ?></textarea>
                        </div>
                    </div>
                    <br>
                    <h4>Foto</h4>
                    <div class="form-group row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                            <div class="table-responsive">
                                <table class="table align-items-center table-flush table-hover">
                                    <thead class="thead-light">
                                        <tr>
                                            <th class="text-center"  width="5%">NO</th>
                                            <th class="text-center"  width="40%">GAMBAR</th>
                                            <th class="text-center"  width="55%"></th>
                                        </tr>
                                        
                                    </thead>
                                    <tbody>
                                        <?php foreach ($detail['list_gambar'] as $key => $value) { ?>
                                        <tr>
                                            <td class="text-center"><?= $key+1 ?></td>
                                            <td class="text-center">
                                                <img class="py-3" width="300" height="300" src="<?= base_url() ?>uploads/<?= $value['img'] ?>">
                                            </td>
                                            <td class="text-center">
                                                <a href="<?= base_url() ?>admin/wisata/delete_gambar/<?= $value['id_gambar'] ?>/<?= $detail['id_wisata'] ?>/<?= $value['img'] ?>" onClick="if(!confirm('Apakah anda yakin akan menghapus?')){return false;}" class="btn btn-danger btn-sm">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <h4>Tambah Foto</h4>
                    <button class="btn btn-primary mb-1" id="add"><i class="fas fa-plus"></i> Tambah</button>
                    <div id="kotak"></div>
                    <div class="form-group row">
                      <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
    <!--Row-->
</div>
</div>
<!---Container Fluid-->
<!-- Footer -->
<?php $ci = $ci = get_instance();
$ci->load->view('templates/footer');
?>

<script>
$(document).ready(function(){
    $('#add').click(function(event){
        var tambahkotak = $('#kotak');
        event.preventDefault(); 
        $('<div class="form-group row"><label for="img" class="col-sm-3 col-form-label">Gambar</label><div class="col-sm-8"><input type="file" name="img[]" class="form-control" id="img" placeholder="img" accept="image/png, image/jpg, image/jpeg" required></div><button id="remove" class="btn btn-danger mb-1" id="add"><i class="fas fa-trash"></i> Hapus</button></div>').appendTo(tambahkotak);     
    });
    
    $('body').on('click','#remove',function(){  
        $(this).parent('div').remove(); 
    });     
});
</script>