<!-- header -->
<?php $ci = $ci = get_instance();
$ci->load->view('templates/header'); ?>
<!-- header -->

<!-- Container Fluid-->
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Master Wisata</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item active" aria-current="page">Wisata</li>
        </ol>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Wisata</h6>
                    <button onClick="location='<?= base_url() ?>admin/wisata/add'" type="button" class="btn btn-primary mb-1"><i class="fas fa-plus"></i> Tambah</button>
                </div>
                <div class="ml-3 mr-3">
                    <?= $this->session->flashdata('pesan'); ?>
                </div>
                <?php unset($_SESSION['pesan']) ?>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush table-hover" id="dataTableHover">
                        <!-- <table width="100%" border="1"> -->
                        <thead class="thead-light">
                            <tr>
                                <th class="text-center">NO</th>
                                <th class="text-center">WISATA</th>
                                <th class="text-center">ALAMAT</th>
                                <th class="text-center">BIAYA</th>
                                <!-- <th class="text-center">SEWA</th> -->
                                <th class="text-center"></th>
                            </tr>
                            
                        </thead>
                        <tbody>
                            <?php $no=1; foreach ($rs_wisata as $key => $wisata) { ?>
                                <tr>
                                    <td class="text-center"><?= $no++ ?></td>
                                    <td class="text-center"><?= $wisata['nama_wisata'] ?></td>
                                    <td class="text-center"><?= $wisata['alamat'] ?></td>
                                    <td class="text-center"><?= $wisata['biaya'] ?></td>
                                    <!-- <td class="text-center"><?= $wisata['deskripsi'] ?></td> -->
                                    <td class="text-center">
                                        <a href="<?= base_url() ?>admin/wisata/edit/<?= $wisata['id_wisata'] ?>" class="btn btn-info btn-sm">
                                            <i class="fas fa-pen"></i>
                                        </a>
                                        <a href="<?= base_url() ?>admin/wisata/delete/<?= $wisata['id_wisata'] ?>/<?= $wisata['id_wisata_img'] ?>" onClick="if(!confirm('Apakah anda yakin akan menghapus?')){return false;}" class="btn btn-danger btn-sm">
                                            <i class="fas fa-trash"></i>
                                        </a>
                                        <a href="<?= base_url() ?>admin/wisata/lihat/<?= $wisata['id_wisata'] ?>" class="btn btn-info btn-sm">
                                            <i class="fas fa-eye"></i>
                                        </a>
                                    </td>
                                <?php } ?>
                                </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!--Row-->
</div>
</div>
<!---Container Fluid-->

<style>
    table thead th.rotasi {
        transform: rotate(-90deg);
        /* transform-origin: top left; */
    }

    .modal-lg {
        max-width: 50% !important;
    }
</style>

<!-- Footer -->
<?php $ci = $ci = get_instance();
$ci->load->view('templates/footer');
?>
<script>

    $(".add").click(function() {
        // var 
        var action = $(this).data("action");
        var title = $(this).data("title");

        $('.modal-title').text(title);
        if (action == 'tambah') {
            $("#action_process").val(action);
            $("#nik").val('').change();
            $("#username").val('');
            $("#password").val('');
            // 
            $("#exampleModal").modal('show');
        }
    });

    $(".edit").click(function() {
        // var 
        var action = $(this).data("action");
        var title = $(this).data("title");

        $('.modal-title').text(title);
        $("#action_process").val(action);
        var id_mobil = $(this).data("id_mobil");
        // get data warga
        $.ajax({
            type: 'POST',
            data: 'id_mobil=' + id_mobil,
            url: '<?= base_url() ?>admin/mobil/detail_mobil',
            dataType: 'JSON',
            success: function(data) {
                console.log(data);

                // $("#nik_edit").val(data.nik).change();
                $("#id_mobil_edit").val(data.id_mobil);
                $("#kapasitas_edit").val(data.kapasitas);
                $("#nama_mobil_edit").val(data.nama_mobil);
                $("#sewa_edit").val(data.sewa);
                $("#tahun_edit").val(data.tahun);
                $("#img_edit").val(data.img);
                $("#img_edit_show").attr('src', '<?= base_url() ?>uploads/'+data.img);
                // 
                $("#exampleModal_edit").modal('show');
            }
        });
    });

    $(".delete").click(function() {
        // var 
        var action = $(this).data("action");
        var title = $(this).data("title");

        $('.modal-title').text(title);
        $("#action_process").val(action);
        var id_user = $(this).data("id_user");
        // get data warga
        $.ajax({
            type: 'POST',
            data: 'id_user=' + id_user,
            url: '<?= base_url() ?>admin/user/detail_user',
            dataType: 'JSON',
            success: function(data) {

                $("#nik_delete").val(data.nik).change();
                $("#role_delete").val(data.role).change();
                $("#username_delete").val(data.username);
                $("#nik_edit_old").val(data.nik);
                $("#id_delete").val(data.id);
                $("#password_delete").val('');
                // 
                $("#exampleModal_delete").modal('show');
            }
        });
    });


</script>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role=" document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/mobil/add_process" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="nama_mobil" class="col-sm-3 col-form-label">Nama Wisata</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_mobil" class="form-control" id="nama_mobil" placeholder="Nama Wisata" value="<?= set_value('nama_mobil'); ?>" required>
                            <?= form_error('nama_mobil', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kapasitas" class="col-sm-3 col-form-label">Kapasitas</label>
                        <div class="col-sm-9">
                            <input type="text" name="kapasitas" class="form-control" id="kapasitas" placeholder="Kapasitas" value="<?= set_value('kapasitas'); ?>" required>
                            <?= form_error('kapasitas', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tahun" class="col-sm-3 col-form-label">Tahun</label>
                        <div class="col-sm-9">
                            <input type="number" name="tahun" class="form-control" id="tahun" placeholder="Tahun" value="<?= set_value('tahun'); ?>" required>
                            <?= form_error('tahun', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sewa" class="col-sm-3 col-form-label">Sewa</label>
                        <div class="col-sm-9">
                            <input type="number" name="sewa" class="form-control" id="sewa" placeholder="Sewa" value="<?= set_value('sewa'); ?>" required>
                            <?= form_error('sewa', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="img" class="col-sm-3 col-form-label">Gambar</label>
                        <div class="col-sm-9">
                            <input type="file" name="img" class="form-control" id="img" placeholder="img" accept="image/png, image/jpg, image/jpeg" value="<?= set_value('img'); ?>" required>
                            <?= form_error('img', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal_edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role=" document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/mobil/edit_process" method="post" enctype="multipart/form-data">
                <input type="text" name="id_mobil" id="id_mobil_edit">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="nama_mobil" class="col-sm-3 col-form-label">Nama Wisata</label>
                        <div class="col-sm-9">
                            <input type="text" name="nama_mobil" class="form-control" id="nama_mobil_edit" placeholder="Nama Wisata" value="<?= set_value('nama_mobil'); ?>" required>
                            <?= form_error('nama_mobil', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kapasitas" class="col-sm-3 col-form-label">Kapasitas</label>
                        <div class="col-sm-9">
                            <input type="text" name="kapasitas" class="form-control" id="kapasitas_edit" placeholder="Kapasitas" value="<?= set_value('kapasitas'); ?>" required>
                            <?= form_error('kapasitas', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="tahun" class="col-sm-3 col-form-label">Tahun</label>
                        <div class="col-sm-9">
                            <input type="number" name="tahun" class="form-control" id="tahun_edit" placeholder="Tahun" value="<?= set_value('tahun'); ?>" required>
                            <?= form_error('tahun', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="sewa" class="col-sm-3 col-form-label">Sewa</label>
                        <div class="col-sm-9">
                            <input type="number" name="sewa" class="form-control" id="sewa_edit" placeholder="Sewa" value="<?= set_value('sewa'); ?>" required>
                            <?= form_error('sewa', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <input type="text" name="img_lama" id="img_edit">
                    <div class="form-group row">
                        <label for="img" class="col-sm-3 col-form-label">Gambar</label>
                        <div class="col-sm-9">
                            <input type="file" name="img_baru" class="form-control" id="img_edit" placeholder="img" accept="image/png, image/jpg, image/jpeg" value="<?= set_value('img'); ?>">
                            <?= form_error('img', '<small class="text-danger">', '</small>'); ?>
                            <small class="text-danger">Kosongi jika tidak ingin merubah gambar</small>'
                        </div>
                    </div>
                    <img id="img_edit_show" width="300" height="300">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal_delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role=" document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"> </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url() ?>admin/user/delete_process" method="post">
                <input type="hidden" name="id" id="id_delete">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="nik_edit" class="col-sm-3 col-form-label">NIK</label>
                        <div class="col-sm-9">
                            <select readonly class="select2-single form-control" name="nik" id="nik_delete" style="width: 100%;">
                                <option value=""></option>
                                <?php foreach ($rs_warga as $key => $warga) { ?>
                                    <option value="<?= $warga['nik'] ?>"><?= $warga['nik'] ?> - <?= $warga['nama'] ?></option>
                                <?php } ?>
                            </select>
                            <?= form_error('nik', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role_delete" class="col-sm-3 col-form-label">Role</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="role" id="role_delete" style="width: 100%;" readonly>
                                <option value="user">User</option>
                                <option value="admin">Admin</option>
                            </select>
                            <?= form_error('role', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="username_edit" class="col-sm-3 col-form-label">Username</label>
                        <div class="col-sm-9">
                            <input type="text" name="username" class="form-control" id="username_delete" placeholder="Username" value="<?= set_value('username'); ?>" readonly>
                            <?= form_error('username', '<small class="text-danger">', '</small>'); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-sm-3 col-form-label">Password</label>
                        <div class="col-sm-9">
                            <input type="password" name="password" class="form-control" id="password_delete" placeholder="Password" value="<?= set_value('password'); ?>" readonly>
                            <?= form_error('password', '<small class="text-danger">', '</small>'); ?>
                            <small class="text-danger">*Kosongi jika tidak ingin merubah</small>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
