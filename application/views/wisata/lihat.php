<!-- <!DOCTYPE HTML>
<html><head>
<title>Unlimited form</title>
</head>
<body>
    <form action="" method="post" id="kotak">
        <textarea id="name" class="name"></textarea>
        <button id="add" class="add">tambah</button>
    </form>
</body>
</html> -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>



<!-- header -->
<?php $ci = $ci = get_instance();
$ci->load->view('templates/header'); ?>
<!-- header -->

<script type="text/javascript" src="<?= base_url() ?>admin/assets/vendor/ckeditor/ckeditor.js"></script>
<!-- Container Fluid-->
<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Wisata <?= $detail['nama_wisata'] ?></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="./">Home</a></li>
            <li class="breadcrumb-item">Detail</li>
            <li class="breadcrumb-item active" aria-current="page">Wisata</li>
        </ol>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- DataTable with Hover -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Review</h6>
                  <button onClick="location='<?= base_url() ?>admin/wisata'" type="button" class="btn btn-primary mb-1"><i class="fas fa-arrow-left"></i> Kembali</button>
                </div>
                <div class="card-body">
                    <h4>Nama Wisata</h4>
                    <p><?= $detail['nama_wisata'] ?></p>
                    <br>
                    <h4>Deskripsi</h4>
                    <p><?= $detail['deskripsi'] ?></p>
                    <br>
                    <h4>Rincian</h4>
                    <table width="50%" class="height">
                        <tr>
                            <td width="20%"> Alamat Wisata</td>
                            <td width="2%"> :</td>
                            <td width="78%"> <?= $detail['alamat'] ?></td>
                        </tr>
                        <tr>
                            <td width="20%"> Biaya</td>
                            <td width="2%"> :</td>
                            <td width="78%"> Rp. <?= number_format( $detail['biaya'] ,0,',','.'); ?></td>
                        </tr>
                    </table>
                    <br>
                    <h4>Foto</h4>
                    <?php foreach ($detail['list_gambar'] as $key => $value) { ?>
                        <img class="py-3" width="400" height="400" src="<?= base_url() ?>uploads/<?= $value['img'] ?>"> 
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <!--Row-->
</div>
</div>
<style>
    table {
      border-collapse: collapse;
      width: 100%;
    }

    th {
      height: 70px;
    }
    tr {
      height: 50px;
    }
</style>
<!---Container Fluid-->
<!-- Footer -->
<?php $ci = $ci = get_instance();
$ci->load->view('templates/footer');
?>