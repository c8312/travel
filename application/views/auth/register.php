<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="img/logo/logo.png" rel="icon">
    <title>RuangAdmin - Register</title>
    <link href="<?= base_url() ?>/admin/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>/admin/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>/admin/assets/css/ruang-admin.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-login">
    <!-- Register Content -->
    <div class="container-login">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
                <div class="card shadow-sm my-5">
                    <div class="card-body p-0">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="login-form">
                                    <div class="text-center">
                                        <?= $this->session->flashdata('pesan'); ?>
                                        <h1 class="h4 text-gray-900 mb-4">Register</h1>
                                    </div>
                                    <form action="<?= base_url() ?>admin/auth/register" method="post">
                                        <div class="form-group">
                                            <label>Nama Lengkap</label>
                                            <input type="text" value="<?= set_value('nama'); ?>" name="nama" class="form-control" id="exampleInputFirstName" placeholder="Masukkan Nama Lengkap" required>
                                            <?= form_error('nama', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat</label>
                                            <input type="text" value="<?= set_value('alamat'); ?>" name="alamat" class="form-control" id="exampleInputFirstName" placeholder="Masukkan Alamat" required>
                                            <?= form_error('alamat', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Username</label>
                                            <input type="text" value="<?= set_value('username'); ?>" name="username" class="form-control" id="exampleInputFirstName" placeholder="Masukkan Username" required>
                                            <?= form_error('username', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" value="<?= set_value('password'); ?>" name="password" class="form-control" id="exampleInputPassword" placeholder="Password" required>
                                            <?= form_error('password', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <label>Ulangi Password</label>
                                            <input type="password" value="<?= set_value('password2'); ?>" name="password2" class="form-control" id="exampleInputPasswordRepeat" placeholder="Repeat Password" required>
                                            <?= form_error('password2', '<small class="text-danger">', '</small>'); ?>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-block">Daftar</button>
                                        </div>
                                        <hr>
                                    </form>
                                    <hr>
                                    <div class="text-center">
                                        <a class="font-weight-bold small" href="<?= base_url() ?>/admin/auth">Already have an account?</a>
                                    </div>
                                    <div class="text-center">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Register Content -->
    <script src="<?= base_url() ?>/admin/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?= base_url() ?>/admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url() ?>/admin/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?= base_url() ?>/admin/assets/js/ruang-admin.min.js"></script>
</body>

</html>