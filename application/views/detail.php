<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <title>Travel - About Us</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/fontawesome.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/templatemo-woox-travel.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css">
    <link rel="stylesheet"href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
<!--

TemplateMo 580 Woox Travel

https://templatemo.com/tm-580-woox-travel

-->
  </head>

<body>

  <!-- ***** Preloader Start ***** -->
  <div id="js-preloader" class="js-preloader">
    <div class="preloader-inner">
      <span class="dot"></span>
      <div class="dots">
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  </div>
  <!-- ***** Preloader End ***** -->

  <!-- ***** Header Area Start ***** -->
  <header class="header-area header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="main-nav">
                    <!-- ***** Logo Start ***** -->
                    <a href="<?= base_url() ?>index.html" class="logo">
                        <img src="<?= base_url() ?>assets/images/logo.png" alt="">
                    </a>
                    <!-- ***** Logo End ***** -->
                    <!-- ***** Menu Start ***** -->
                    <ul class="nav">
                        <li><a href="<?= base_url() ?>index.html">Home</a></li>
                        <li><a href="<?= base_url() ?>about.html" class="active">About</a></li>
                        <li><a href="<?= base_url() ?>deals.html">Deals</a></li>
                        <li><a href="<?= base_url() ?>reservation.html">Reservation</a></li>
                        <li><a href="<?= base_url() ?>reservation.html">Book Yours</a></li>
                    </ul>   
                    <a class='menu-trigger'>
                        <span>Menu</span>
                    </a>
                    <!-- ***** Menu End ***** -->
                </nav>
            </div>
        </div>
    </div>
  </header>
  <!-- ***** Header Area End ***** -->

  <!-- ***** Main Banner Area Start ***** -->
  <div class="about-main-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <!-- <div class="content">
            <div class="blur-bg"></div>
            <h4>EXPLORE OUR COUNTRY</h4>
            <div class="line-dec"></div>
            <h2>Welcome To Caribbean</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt uttersi labore et dolore magna aliqua is ipsum suspendisse ultrices gravida</p>
            <div class="main-button">
              <a href="<?= base_url() ?>reservation.html">Discover More</a>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <!-- ***** Main Banner Area End ***** -->
  
  <div class="cities-town">
    <div class="container">
      <div class="row">
        <div class="slider-content">
          <div class="row">
            <div class="col-lg-12">
              <h2><?= $detail['nama_wisata'] ?> <em><?= $detail['alamat'] ?></em></h2>
            </div>
            <div class="col-lg-12">
              <div class="owl-cites-town owl-carousel">
                <?php foreach ($detail['list_gambar'] as $key => $value) { ?>
                  <div class="item">
                    <div class="thumb">
                      <img src="<?= base_url() ?>uploads/<?= $value['img'] ?>" alt="">
                      <h4><?= $detail['nama_wisata'] ?></h4>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <div class="visit-country">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="items">
            <div class="row">
              <div class="col-lg-12">
                <div class="item">
                  <div class="row">
                    <div class="col-lg-4 col-sm-5">
                      <div class="image">
                        <img src="<?= base_url() ?>uploads/<?= $detail['list_gambar'][0]['img']; ?>" alt="">
                      </div>
                    </div>
                    <div class="col-lg-8 col-sm-7">
                      <div class="right-content">
                        <h4><?= $detail['nama_wisata'] ?></h4>
                        <span><?= $detail['alamat'] ?></span>
                        <p><?= $detail['deskripsi'] ?></p>
                        <!-- <ul class="info">
                          <li><i class="fa fa-user"></i> 7 Orang</li>
                          <li><i class="fa fa-globe"></i> 41.290 km2</li>
                          <li><i class="fa fa-home"></i> $1.100.200</li>
                        </ul> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="weekly-offers">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 offset-lg-3">
          <div class="section-heading text-center">
            <h2>Silahkan pilih kendaraan yang akan digunakan</h2>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p> -->
          </div>
        </div>
      </div>
    </div>
    
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="owl-weekly-offers owl-carousel">
            <?php foreach ($rs_mobil as $key => $value) { ?>
              
              <div class="item">
                <div class="thumb">
                  <img src="<?= base_url() ?>uploads/<?= $value['img'] ?>" alt="">
                  <div class="text">
                    <h4><?= $value['nama_mobil'] ?> <br></h4>
                    <h6> Rp. <?= number_format( $value['sewa'] ,0,',','.'); ?><br></h6>
                    <div class="line-dec"></div>
                    <ul>
                      <li>Fasilitas:</li>
                      <li><i class="fa fa-taxi"></i> Tahun <?= $value['tahun'] ?></li>
                      <li><i class="fa fa-users"></i> Jumlah penumpang <?= $value['kapasitas'] ?></li>
                      <!-- <li><i class="fa fa-ticket"></i> Daily Places Visit</li> -->
                    </ul>
                    <div class="main-button">
                      <a href="<?= base_url() ?>check_out/<?= $detail['id_wisata'] ?>/<?= $value['id_mobil'] ?>">Pilih Kendaraan</a>
                    </div>
                  </div>
                </div>
              </div>
            <?php } ?>
            
            
          </div>
        </div>
      </div>
    </div>
  </div>
<!--   <div class="more-about">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 align-self-center">
          <div class="left-image">
            <img src="<?= base_url() ?>assets/images/about-left-image.jpg" alt="">
          </div>
        </div>
        <div class="col-lg-6">
          <div class="section-heading">
            <h2>Discover More About Our Country</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
          </div>
          <div class="row">
            <div class="col-lg-6">
              <div class="info-item">
                <h4>150.640 +</h4>
                <span>Total Guests Yearly</span>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="info-item">
                <h4>175.000+</h4>
                <span>Amazing Accomoditations</span>
              </div>
            </div>
            <div class="col-lg-12">
              <div class="info-item">
                <div class="row">
                  <div class="col-lg-6">
                    <h4>12.560+</h4>
                    <span>Amazing Places</span>
                  </div>
                  <div class="col-lg-6">
                    <h4>240.580+</h4>
                    <span>Different Check-ins Yearly</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
          <div class="main-button">
            <a href="<?= base_url() ?>reservation.html">Discover More</a>
          </div>
        </div>
      </div>
    </div>
  </div> -->
  <div class="call-to-action">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <h2>Apakah Anda Ingin Bepergian ?</h2>
          <h4>Pilih Kendaraan</h4>
        </div>
        <div class="col-lg-4">
          <div class="border-button">
            <a href="<?= base_url() ?>reservation.html">Order sekarang..</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p>Copyright © 2022 <a href="<?= base_url() ?>#">WoOx Travel</a> Company. All rights reserved. 
          <br>Design: <a href="https://templatemo.com" target="_blank" title="free CSS templates">TemplateMo</a></p>
        </div>
      </div>
    </div>
  </footer>


  <!-- Scripts -->
  <!-- Bootstrap core JavaScript -->
  <script src="<?= base_url() ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>vendor/bootstrap/js/bootstrap.min.js"></script>

  <script src="<?= base_url() ?>assets/js/isotope.min.js"></script>
  <script src="<?= base_url() ?>assets/js/owl-carousel.js"></script>
  <script src="<?= base_url() ?>assets/js/wow.js"></script>
  <script src="<?= base_url() ?>assets/js/tabs.js"></script>
  <script src="<?= base_url() ?>assets/js/popup.js"></script>
  <script src="<?= base_url() ?>assets/js/custom.js"></script>

  <script>
    $(".option").click(function(){
      $(".option").removeClass("active");
      $(this).addClass("active"); 
    });
  </script>

  </body>

</html>
