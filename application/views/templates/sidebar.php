<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url() ?>">
        <div class="sidebar-brand-icon">
            <img src="<?= base_url() ?>assets/img/logo/logo2.png">
        </div>
        <div class="sidebar-brand-text mx-3">Travel</div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url() ?>">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Features
    </div>
    <?php if ($role == 'admin') { ?>
        <li class="nav-item <?php if ($title == 'Mutasi' || $title == 'Log Mutasi') { ?><?= "active" ?><?php } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseForm" aria-expanded="true" aria-controls="collapseForm">
                <i class="fab fa-fw fa-wpforms"></i>
                <span>Mutasi</span>
            </a>
            <div id="collapseForm" class="collapse <?php if ($title == 'Mutasi' || $title == 'Log Mutasi') { ?><?= "show" ?><?php } ?>" aria-labelledby="headingForm" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Mutasi</h6>
                    <a class="collapse-item <?php if ($title == 'Mutasi') { ?><?= "active" ?><?php } ?>" href="<?= base_url() ?>admin/mutasi">Mutasi</a>
                    <a class="collapse-item <?php if ($title == 'Log Mutasi') { ?><?= "active" ?><?php } ?>" href="<?= base_url() ?>admin/log_mutasi">Log Mutasi</a>
                </div>
            </div>
        </li>
        <li class="nav-item <?php if ($title == 'Warga' || $title == 'Lahan') { ?><?= "active" ?><?php } ?>">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseForm" aria-expanded="true" aria-controls="collapseForm">
                <i class="fab fa-fw fa-wpforms"></i>
                <span>Data Master</span>
            </a>
            <div id="collapseForm" class="collapse <?php if ($title == 'Mobil' || $title == 'Wisata' | $title == 'User') { ?><?= "show" ?><?php } ?>" aria-labelledby="headingForm" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Data Master</h6>
                    <a class="collapse-item <?php if ($title == 'Mobil') { ?><?= "active" ?><?php } ?>" href="<?= base_url() ?>admin/mobil">Data mobil</a>
                    <a class="collapse-item <?php if ($title == 'Wisata') { ?><?= "active" ?><?php } ?>" href="<?= base_url() ?>admin/wisata">Data Wisata</a>
                    <a class="collapse-item <?php if ($title == 'User') { ?><?= "active" ?><?php } ?>" href="<?= base_url() ?>admin/user">Data User</a>
                </div>
            </div>
        </li>
    <?php } ?>

    <hr class="sidebar-divider">
    <div class="version" id="version-ruangadmin"></div>
</ul>
<div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
        <!-- Sidebar -->