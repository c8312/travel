<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">
            <span>copyright &copy; <script>
                    document.write(new Date().getFullYear());
                </script> - developed by
                <b><a href="https://indrijunanda.gitlab.io/" target="_blank">indrijunanda</a></b>
            </span>
        </div>
    </div>
</footer>
<!-- Footer -->
</div>
</div>

<!-- Scroll to top -->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Modal Logout -->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabelLogout">Ohh No!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah kamu yakin ingin keluar?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Cancel</button>
                <a href="<?= base_url() ?>auth/logout" class="btn btn-primary">Logout</a>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url() ?>admin/assets/vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url() ?>admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>admin/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?= base_url() ?>admin/assets/js/ruang-admin.min.js"></script>
<script src="<?= base_url() ?>admin/assets/vendor/chart.js/Chart.min.js"></script>
<script src="<?= base_url() ?>admin/assets/js/demo/chart-area-demo.js"></script>
<script src="<?= base_url() ?>admin/assets/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>admin/assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url() ?>admin/assets/vendor/select2/dist/js/select2.min.js"></script>
<!-- Bootstrap Datepicker -->
<script src="<?= base_url() ?>admin/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
</body>
<script>
    $(document).ready(function() {
        $('#dataTableHover').DataTable(); // ID From dataTable with Hover
        // select2
        $('.select2-single').select2({
            placeholder: "Pilih Warga",
            allowClear: true
        });
        // Bootstrap Date Picker
        $('#simple-date1 .input-group.date').datepicker({
            format: 'yyyy-mm-dd',
            todayBtn: 'linked',
            todayHighlight: true,
            autoclose: true,
        });
    });

    $(".itemSearch").select2();
</script>

</html>