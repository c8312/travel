<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="<?= base_url() ?>admin/assets/img/logo/logo.png" rel="icon">
    <title>Travel - Dashboard</title>
    <link href="<?= base_url() ?>admin/assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>admin/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url() ?>admin/assets/css/ruang-admin.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>admin/assets/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>admin/assets/vendor/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap DatePicker -->
    <link href="<?= base_url() ?>admin/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
</head>

<body id="page-top">
    <div id="wrapper">

        <!-- Sidebar -->
        <?php $ci = $ci = get_instance();
        $ci->load->view('templates/sidebar'); ?>
        <!-- Sidebar -->

        <!-- TopBar -->
        <?php $ci = $ci = get_instance();
        $ci->load->view('templates/topbar');
        ?>
        <!-- Topbar -->